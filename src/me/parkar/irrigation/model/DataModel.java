package me.parkar.irrigation.model;

import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

@PersistenceCapable
public class DataModel {

	@PrimaryKey
    @Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private String key;
	
	@Persistent
	private String moisture;
	
	@Persistent
	private String temperature;
	
	@Persistent
	private String status;
	
	public DataModel() {
		this.key = "1";
	}

	public DataModel(String moisture, String temperature) {
		super();
		this.key = "1";
		this.moisture = moisture;
		this.temperature = temperature;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getMoisture() {
		return moisture;
	}

	public void setMoisture(String moisture) {
		this.moisture = moisture;
	}

	public String getTemperature() {
		return temperature;
	}

	public void setTemperature(String temperature) {
		this.temperature = temperature;
	}


	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "DataModel [key=" + key + ", moisture=" + moisture + ", temperature=" + temperature + ", status="
				+ status + "]";
	}
	
	
	
}
