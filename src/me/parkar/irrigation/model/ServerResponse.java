package me.parkar.irrigation.model;

/**
 * @author basitparkar
 */
public class ServerResponse {

    int errorCode;
    String errorDesc;
    String data;

    public ServerResponse(int errorCode, String errorDesc, String data) {
        this.errorCode = errorCode;
        this.errorDesc = errorDesc;
        this.data = data;
    }
}
