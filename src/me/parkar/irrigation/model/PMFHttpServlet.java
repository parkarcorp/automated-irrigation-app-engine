
package me.parkar.irrigation.model;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.List;

import javax.jdo.JDOHelper;
import javax.jdo.JDOObjectNotFoundException;
import javax.jdo.PersistenceManager;
import javax.jdo.PersistenceManagerFactory;
import javax.jdo.Query;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;

/**
 * @author basitparkar
 */
public abstract class PMFHttpServlet extends HttpServlet {

    private static final PersistenceManagerFactory pmfInstance =
            JDOHelper.getPersistenceManagerFactory("transactions-optional");

    protected static PersistenceManagerFactory getPMF() {
        return pmfInstance;
    }

    protected boolean doPersistent(Object persistentObject) {
        PersistenceManager persistenceManager = getPMF().getPersistenceManager();

        try {
            persistenceManager.makePersistent(persistentObject);
        } catch (Exception e) {
            return false;
        } finally {
            persistenceManager.close();
        }

        return true;
    }

    protected<T extends Object> T getPersistentObject(Class<T> classObj, Object key) {
        PersistenceManager pm = getPMF().getPersistenceManager();

        try {
        	return pm.getObjectById(classObj, key);
        } catch(JDOObjectNotFoundException e) {
        	
        }
        
        return null;
    }
    
    protected<T extends Object> List<T> getPersistentObjects(Class<T> classObj) {
        PersistenceManager pm = getPMF().getPersistenceManager();
        Query query = pm.newQuery(classObj);

        List<T> data = (List<T>) query.execute();

        return data;
    }

    protected<T extends Object> List<T> getPersistentObjects(Class<T> classObj,  String whereQuery, String parameterTypes, String param1) {
        PersistenceManager pm = getPMF().getPersistenceManager();
        Query query = pm.newQuery(classObj);
        query.setFilter(whereQuery);
        query.declareParameters(parameterTypes);

        List<T> data = (List<T>) query.execute(param1);

        return data;
    }

    protected String getRequestString(HttpServletRequest request) {
        String data;
        StringBuilder builder = new StringBuilder(request.getContentLength());
        try {
        	BufferedReader reader = request.getReader();
            while ((data = reader.readLine()) != null) {
                builder.append(data);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return builder.toString();
    }
}
