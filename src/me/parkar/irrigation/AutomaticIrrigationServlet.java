package me.parkar.irrigation;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import me.parkar.irrigation.model.DataModel;
import me.parkar.irrigation.model.PMFHttpServlet;
import me.parkar.irrigation.model.ServerResponse;
import me.parkar.irrigation.util.StringUtils;

@SuppressWarnings("serial")
public class AutomaticIrrigationServlet extends PMFHttpServlet {

	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		String jsonData = getRequestString(req);
		ServerResponse response;

		if (!StringUtils.isNullOrEmpty(jsonData)) {

			DataModel model = new Gson().fromJson(jsonData, DataModel.class);
			DataModel temp = getPersistentObject(DataModel.class, model.getKey());
			if (temp == null) {
				model.setStatus("off");
			} else {
				model.setStatus(temp.getStatus());
			}

			boolean updated = doPersistent(model);

			if (updated) {
				response = new ServerResponse(0, null, model.getStatus());
			} else {
				response = new ServerResponse(2, "Failed to update record", null);
			}

		} else {
			response = new ServerResponse(1, "Invalid Request", null);
		}

		resp.getWriter().write(new Gson().toJson(response));
	}

}
