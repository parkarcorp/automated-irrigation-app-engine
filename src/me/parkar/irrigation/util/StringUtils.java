package me.parkar.irrigation.util;

/**
 * @author basitparkar
 */
public class StringUtils {

    public static boolean isNullOrEmpty(String value) {
        if(value == null) {
            return true;
        }

        if(value.isEmpty()) {
            return true;
        }

        return false;
    }
}
