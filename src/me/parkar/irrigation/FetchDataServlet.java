package me.parkar.irrigation;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import me.parkar.irrigation.model.DataModel;
import me.parkar.irrigation.model.PMFHttpServlet;
import me.parkar.irrigation.model.ServerResponse;

public class FetchDataServlet extends PMFHttpServlet {
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		DataModel model = getPersistentObject(DataModel.class, "1");

		ServerResponse response;
		if (model != null) {
			response = new ServerResponse(0, null, new Gson().toJson(model));
		} else {
			response = new ServerResponse(1, "No Data", null);
		}

		resp.getWriter().write(new Gson().toJson(response));
	}
}
